import {shallowMount} from "@vue/test-utils";
import GoogleMap from "@/lib-components/subcomponents/GoogleMap.vue";

import {mapItem} from "@/interfaces/Interfaces";
import {mapItems} from "@/mocks/mapItemsMock";
import {mapConfig} from "@/interfaces/configInterfaces";

describe('GoogleMap.vue tests', () => {
    it('renders prop config default width', () => {
        const items = <mapItem[]>[...mapItems];
        const config = <mapConfig> {mapModel: {name: 'google', apiKey: '123456'}, zoom: '5'}
        const wrapper = shallowMount(GoogleMap, {
            props: {items: items, config: config}
        })
        expect(wrapper.vm.width).toBe('600px')
    })

    it('renders prop config custom width', () => {
        const items = <mapItem[]>[...mapItems];
        const config = <mapConfig> {mapModel: {name: 'google', apiKey: '123456'}, zoom: '5', width: '800'}
        const wrapper = shallowMount(GoogleMap, {
            props: {items: items, config: config}
        })
        expect(wrapper.vm.width).toBe('800px')
    })

    it('renders prop config default height', () => {
        const items = <mapItem[]>[...mapItems];
        const config = <mapConfig> {mapModel: {name: 'google', apiKey: '123456'}, zoom: '5'}
        const wrapper = shallowMount(GoogleMap, {
            props: {items: items, config: config}
        })
        expect(wrapper.vm.height).toBe('600px')
    })

    it('renders prop config custom height', () => {
        const items = <mapItem[]>[...mapItems];
        const config = <mapConfig> {mapModel: {name: 'google', apiKey: '123456'}, zoom: '5', height: '800'}
        const wrapper = shallowMount(GoogleMap, {
            props: {items: items, config: config}
        })
        expect(wrapper.vm.height).toBe('800px')
    })
})
