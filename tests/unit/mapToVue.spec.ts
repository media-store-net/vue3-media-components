import {shallowMount} from '@vue/test-utils'
import MapToVue from "@/lib-components/mapToVue.vue";
import {mapItems} from "@/mocks/mapItemsMock";
import {mapConfig} from "@/interfaces/configInterfaces";
import {mapItem} from "@/interfaces/Interfaces";

describe('MapToVue.vue', () => {
    //TODO Snapshot test if the component ist final
    /*it('snapshot test', () => {
        const items = <mapItem[]>[...mapItems];
        const config = <mapConfig>{mapModel: {name: 'google', apiKey: '123456'}}
        const wrapper = shallowMount(MapToVue, {
            props: {items: items, config: config}
        })
        expect(wrapper.element).toMatchSnapshot()
    })*/

    it('renders prop items when passed', () => {
        const items = <mapItem[]>[...mapItems];
        const config = <mapConfig>{mapModel: {name: 'google', apiKey: '123456'}}
        const wrapper = shallowMount(MapToVue, {
            props: {items: items, config: config}
        })
        expect(wrapper.vm.$props.items).toMatchObject(items)
    })

    it('renders prop config when passed', () => {
        const items = <mapItem[]>[...mapItems];
        const config = <mapConfig>{mapModel: {name: 'google', apiKey: '123456'}}
        const wrapper = shallowMount(MapToVue, {
            props: {items: items, config: config}
        })
        expect(wrapper.vm.$props.config).toMatchObject(config)
    })


})
