import {mapCoordinates, mapItem} from "@/interfaces/Interfaces";

export default class MapItem implements mapItem {
    id: number | string;
    thumbnailSrc: string;
    title: string;
    subtitle? = '';
    description? = '';
    coordinates: mapCoordinates = {};

    constructor(prop: mapItem) {
        this.id = prop.id;
        this.title = prop.title;
        this.subtitle = prop.subtitle;
        this.description = prop.description;
        this.thumbnailSrc = prop.thumbnailSrc;
        this.coordinates = prop.coordinates;
    }
}
