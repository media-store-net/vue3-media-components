import MapItem from "@/classes/mapItem";

export const mapItems = [
  new MapItem({
    id: 1,
    title: "2-Zi.-Wohnung",
    thumbnailSrc: "https://klike.net/uploads/posts/2020-01/1579858769_1.jpg",
    coordinates: { city: "Bismarckstr. 51, Darmstadt-Nord, 64293 Darmstadt" },
    description:
      "belebte städtische Lage, vornehmlich Mehrfamilienhausbebauung im Wohnumfeld, Wohnstraße mit Gewerbeanteil, Angebot an Dienstleistungen in der Nachbarschaft vorhanden, verkehrsgünstige Lage",
    subtitle: "Schöner Wohnen",
  }),
  new MapItem({
    id: 2,
    title: "Haus 2",
    thumbnailSrc: "",
    coordinates: { lat: "25.344", long: "131.031" },
  }),
  new MapItem({
    id: 3,
    title: "Wohnung 1",
    thumbnailSrc: "",
    coordinates: {},
  }),
  new MapItem({
    id: 4,
    title: "Wohnung 2",
    thumbnailSrc: "",
    coordinates: {},
  }),
  new MapItem({
    id: 5,
    title: "Zimmer XYZ",
    thumbnailSrc: "",
    coordinates: {},
  }),
];
