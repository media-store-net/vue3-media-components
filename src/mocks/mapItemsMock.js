import MapItem from "@/classes/mapItem";
export const mapItems = [
    new MapItem(1, "Haus 1", "", { city: "Darmstadt" }),
    new MapItem(2, "Haus 2", "", { lat: "25.344", long: "131.031" }),
    new MapItem(3, "Wohnung 1", "", {}),
    new MapItem(4, "Wohnung 2", "", {}),
    new MapItem(5, "Zimmer XYZ", "", {}),
];
//# sourceMappingURL=mapItemsMock.js.map